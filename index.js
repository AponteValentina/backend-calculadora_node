//Importar la clase express

import express from 'express';


//Crear un objeto express
const app=express();
const puerto=3000;

// Crear una ruta

app.get("/bienvenida", (req, res)=>{

    res.send("Bienvenido al mundo de backend Node.");
});

//Ruta que nos permite la suma de dos numeros 

app.get("/sumar", (peticion, respuesta)=>{

    let resultado=Number(peticion.query.a)+Number(peticion.query.b);

    respuesta.send("El resultado de la suma de los numeros indicados es: "+resultado.toString());
});

//Ruta que nos permite la resta de dos numeros 

app.get("/restar", (peticion, respuesta)=>{

    let resultado=Number(peticion.query.a)-Number(peticion.query.b);

    respuesta.send("El resultado de la resta de los numero indicados es: "+resultado.toString());
});

//Ruta que nos permite la multiplicacion de dos numeros 

app.get("/multiplicar", (peticion, respuesta)=>{

    let resultado=Number(peticion.query.a)*Number(peticion.query.b);

    respuesta.send("El resultado de la multiplicacion de los numero indicados es: "+resultado.toString());
});

//Ruta que nos permite la divicion de dos numeros 

app.get("/dividir", (peticion, respuesta)=>{

    if(Number(peticion.query.b)!=0){
    let resultado=Number(peticion.query.a)/Number(peticion.query.b);
    respuesta.send("El resultado de la multiplicacion de los numero indicados es: "+resultado.toString());
}
if(Number(peticion.query.b)==0){
    respuesta.send("Math ERROR.....No es posible dividir entre cero");
}
});

//Operacion modulo
app.get("/modular", (peticion, respuesta)=>{

    let resultado=Number(peticion.query.a)%Number(peticion.query.b);

    respuesta.send("El residuo de la divicion de los numeros es: "+resultado.toString());
});

//Inicializar el servidor node

app.listen(puerto, ()=>{console.log("Esta funcionando el servidor.")});